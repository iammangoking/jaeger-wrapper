module gitlab.com/iammangoking/jaeger-wrapper

go 1.16

require (
	github.com/99designs/gqlgen v0.17.1
	github.com/99designs/gqlgen-contrib v0.1.1-0.20200601100547-7a955d321bbd
	github.com/ThreeDotsLabs/watermill v1.2.0-rc.9
	github.com/ThreeDotsLabs/watermill-amqp v1.1.4
	github.com/ThreeDotsLabs/watermill-nats v1.0.7
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis/v8 v8.11.4
	github.com/grpc-ecosystem/grpc-opentracing v0.0.0-20180507213350-8e809c8a8645
	github.com/jinzhu/gorm v1.9.16
	github.com/labstack/echo/v4 v4.7.0
	github.com/laststem/go-opentracing-redis v0.0.0-20210414163347-e7fe5ac90582
	github.com/nats-io/stan.go v0.10.2
	github.com/opentracing/opentracing-go v1.2.0
	github.com/rs/xid v1.3.0
	github.com/rs/zerolog v1.26.1
	github.com/spf13/cobra v1.3.0
	github.com/uber/jaeger-client-go v2.30.0+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	github.com/vektah/gqlparser/v2 v2.4.1
	go.opentelemetry.io/otel v1.4.1
	go.uber.org/atomic v1.9.0 // indirect
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.2
	gorm.io/plugin/opentracing v0.0.0-20211220013347-7d2b2af23560
)
