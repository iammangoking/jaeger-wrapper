package jaeger

import (
	"errors"
	"io"

	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog"
	jaegerConfig "github.com/uber/jaeger-client-go/config"

	"gitlab.com/iammangoking/jaeger-wrapper/config"
	"gitlab.com/iammangoking/jaeger-wrapper/internel/log"
)

var closer io.Closer

func InitClient(cfg *config.Config, logger *zerolog.Logger) (func() error, error) {
	if cfg == nil {
		cfg = config.Disable()
	}
	tracer, flush, err := cfg.NewTracer(
		jaegerConfig.Logger(&log.Logger{Logger: logger}),
	)
	if err != nil {
		return nil, err
	}

	closer = flush
	opentracing.SetGlobalTracer(tracer)

	return Close, nil
}

func Close() error {
	if closer == nil {
		return errors.New("duplicate close")
	}
	closer.Close()
	closer = nil
	return nil
}
