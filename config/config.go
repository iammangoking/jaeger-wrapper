package config

import (
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

type Config = config.Configuration
type SamplerConfig = config.SamplerConfig
type ReporterConfig = config.ReporterConfig
type HeadersConfig = jaeger.HeadersConfig
type BaggageRestrictionsConfig = config.BaggageRestrictionsConfig
type ThrottlerConfig = config.ThrottlerConfig

func FromEnv() *Config {
	cfg, err := config.FromEnv()
	if err != nil {
		return Disable()
	}
	return cfg
}

func Disable() *Config {
	return &Config{
		Disabled: true,
	}
}
