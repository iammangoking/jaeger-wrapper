# jaeger

## Jaeger server local deployment

Use ./deploy/docker-compose.yml

## Usage

As ./example

## Config

Yml tag | Property(or env tag) | Description
--- | --- | ---
serviceName | JAEGER_SERVICE_NAME | The service name.
tags | JAEGER_TAGS | A comma separated list of `name=value` tracer-level tags, which get added to all reported spans. The value can also refer to an environment variable using the format `${envVarName:defaultValue}`.
traceid_128bit | JAEGER_TRACEID_128BIT | Whether to enable 128bit trace-id generation, `true` or `false`. If not enabled, the SDK defaults to 64bit trace-ids.
disabled | JAEGER_DISABLED | Whether the tracer is disabled or not. If `true`, the `opentracing.NoopTracer` is used (default `false`).
rpc_metrics | JAEGER_RPC_METRICS | Whether to store RPC metrics, `true` or `false` (default `false`).
reporter.localAgentHostPort | JAEGER_AGENT_HOST | The hostname for communicating with agent via UDP (default `localhost`).
reporter.localAgentHostPort(*) | JAEGER_AGENT_PORT | The port for communicating with agent via UDP (default `6831`).
reporter.collectorEndpoint | JAEGER_ENDPOINT | The HTTP endpoint for sending spans directly to a collector, i.e. http://jaeger-collector:14268/api/traces. If specified, the agent host/port are ignored.
reporter.user | JAEGER_USER | Username to send as part of "Basic" authentication to the collector endpoint.
reporter.password | JAEGER_PASSWORD | Password to send as part of "Basic" authentication to the collector endpoint.
reporter.logSpans | JAEGER_REPORTER_LOG_SPANS | Whether the reporter should also log the spans" `true` or `false` (default `false`).
reporter.queueSize | JAEGER_REPORTER_MAX_QUEUE_SIZE | The reporter's maximum queue size (default `100`).
`-` | JAEGER_REPORTER_FLUSH_INTERVAL | The reporter's flush interval, with units, e.g. `500ms` or `2s` ([valid units](https://pkg.go.dev/time#ParseDuration); default `1s`).
reporter.disableAttemptReconnecting | JAEGER_REPORTER_ATTEMPT_RECONNECTING_DISABLED | When true, disables udp connection helper that periodically re-resolves the agent's hostname and reconnects if there was a change (default `false`).
`-` | JAEGER_REPORTER_ATTEMPT_RECONNECT_INTERVAL | Controls how often the agent client re-resolves the provided hostname in order to detect address changes ([valid units](https://pkg.go.dev/time#ParseDuration); default `30s`).
sampler.type | JAEGER_SAMPLER_TYPE | The sampler type: `remote`, `const`, `probabilistic`, `ratelimiting` (default `remote`). See also https://www.jaegertracing.io/docs/latest/sampling/.
sampler.param | JAEGER_SAMPLER_PARAM | The sampler parameter (number).
sampler.samplingServerURL | JAEGER_SAMPLING_ENDPOINT | The URL for the sampling configuration server when using sampler type `remote` (default `http://127.0.0.1:5778/sampling`).
sampler.maxOperations | JAEGER_SAMPLER_MAX_OPERATIONS | The maximum number of operations that the sampler will keep track of (default `2000`).
sampler.samplingRefreshInterval | JAEGER_SAMPLER_REFRESH_INTERVAL | How often the `remote` sampler should poll the configuration server for the appropriate sampling strategy, e.g. "1m" or "30s" ([valid units](https://pkg.go.dev/time#ParseDuration); default `1m`).

(*)Combined with `JAEGER_AGENT_HOST:JAEGER_AGENT_PORT`