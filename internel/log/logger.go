package log

import (
	"github.com/rs/zerolog"
)

type Logger struct {
	*zerolog.Logger
}

// Error logs a message at error priority
func (l *Logger) Error(msg string) {
	l.Logger.Error().Msg(msg)
}

// Infof logs a message at info priority
func (l *Logger) Infof(msg string, args ...interface{}) {
	// Info太高了, 降個
	l.Logger.Debug().Msgf(msg, args...)
}
