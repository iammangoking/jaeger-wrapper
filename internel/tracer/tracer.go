package tracer

import (
	"context"
	"net/http"

	"github.com/opentracing/opentracing-go"
)

func ExtractText(m map[string]string) (opentracing.SpanContext, error) {
	return opentracing.GlobalTracer().Extract(
		opentracing.TextMap,
		opentracing.TextMapCarrier(m),
	)
}

func ExtractHeader(h http.Header) (opentracing.SpanContext, error) {
	return opentracing.GlobalTracer().Extract(
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(h),
	)
}

func InjectText(ctx context.Context, m map[string]string) error {
	return opentracing.GlobalTracer().Inject(
		opentracing.SpanFromContext(ctx).Context(),
		opentracing.TextMap,
		opentracing.TextMapCarrier(m),
	)
}

func InjectHeader(ctx context.Context, h http.Header) error {
	return opentracing.GlobalTracer().Inject(
		opentracing.SpanFromContext(ctx).Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(h),
	)
}
