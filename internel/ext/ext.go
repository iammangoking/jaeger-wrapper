package ext

import (
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

type consumerOption struct {
	clientContext opentracing.SpanContext
}

func (r consumerOption) Apply(o *opentracing.StartSpanOptions) {
	if r.clientContext != nil {
		opentracing.ChildOf(r.clientContext).Apply(o)
	}
	ext.SpanKindConsumer.Apply(o)
}

// onsumerOption returns a StartSpanOption appropriate for an RPC consumer span
// with `client` representing the metadata for the remote peer Span if available.
// In case client == nil, due to the client not being instrumented, this RPC
// consumer span will be a root span.
func ConsumerOption(client opentracing.SpanContext) opentracing.StartSpanOption {
	return consumerOption{client}
}
