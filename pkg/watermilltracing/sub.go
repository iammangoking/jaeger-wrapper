package watermilltracing

import (
	"context"
	"errors"

	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/opentracing/opentracing-go"
	otext "github.com/opentracing/opentracing-go/ext"

	"gitlab.com/iammangoking/jaeger-wrapper/internel/ext"
)

const (
	msg_middleware_error_key = "middleware_error"
)

func WrapSub(s message.Subscriber) message.Subscriber {
	return &subscriber{Subscriber: s}
}

type subscriber struct {
	message.Subscriber
}

func (p *subscriber) Subscribe(ctx context.Context, topic string) (<-chan *message.Message, error) {
	messages, err := p.Subscriber.Subscribe(ctx, topic)
	if err != nil {
		return messages, err
	}
	retMsgChan := make(chan *message.Message)
	go func() {
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			case <-msg.Context().Done():
				return
			default:
			}

			// span
			tracer := opentracing.GlobalTracer()
			spctx, _ := tracer.Extract(
				opentracing.TextMap,
				opentracing.TextMapCarrier(msg.Metadata),
			)
			sp := tracer.StartSpan("sub: "+topic, ext.ConsumerOption(spctx))
			msg.SetContext(opentracing.ContextWithSpan(msg.Context(), sp))
			sp.SetTag("msg.UUID", msg.UUID)
			for k, v := range msg.Metadata {
				sp.SetTag("msg.Metadata."+k, v)
			}
			sp.SetTag("msg.Payload", string(msg.Payload))

			retMsgChan <- msg

			select {
			case <-msg.Acked():
				// ack received
			case <-msg.Nacked():
				// nack received
				otext.Error.Set(sp, true)
			}
			if err := msg.Metadata.Get(msg_middleware_error_key); err != "" {
				otext.LogError(sp, errors.New(err))
			}

			// span close
			sp.Finish()
		}
	}()
	return retMsgChan, nil
}
