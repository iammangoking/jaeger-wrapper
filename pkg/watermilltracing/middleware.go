package watermilltracing

import (
	"context"
	"reflect"
	"runtime"
	"strings"

	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

type ctxKey string

func WrapMiddlewares(ms ...message.HandlerMiddleware) []message.HandlerMiddleware {
	return Wrap{}.Middlewares(ms...)
}

func TraceWithSpan(m message.HandlerMiddleware) message.HandlerMiddleware {
	return Trace{}.WithSpan(m)
}

type Wrap struct {
}

func (w Wrap) Middlewares(ms ...message.HandlerMiddleware) []message.HandlerMiddleware {
	var rs []message.HandlerMiddleware

	for _, mw := range ms {
		mp := func(m message.HandlerMiddleware) message.HandlerMiddleware {
			return func(h message.HandlerFunc) message.HandlerFunc {
				return func(msg *message.Message) ([]*message.Message, error) {
					// next middleware
					r, err := m(h)(msg)
					if err != nil {
						msg.Metadata.Set(msg_middleware_error_key, err.Error())
					} else {
						delete(msg.Metadata, msg_middleware_error_key)
					}
					return r, err
				}
			}
		}(mw)

		// return list
		rs = append(rs, mp)
	}

	// for tracing HandlerFunc
	rs = append(rs, TraceWithSpan(handler{}.Middleware))

	return rs
}

type Trace struct{}

func (w Trace) WithSpan(m message.HandlerMiddleware) message.HandlerMiddleware {
	packName := runtime.FuncForPC(reflect.ValueOf(m).Pointer()).Name()
	packs := strings.Split(packName, `/`)
	title := packs[len(packs)-1]
	return func(h message.HandlerFunc) message.HandlerFunc {
		return func(h message.HandlerFunc) message.HandlerFunc {
			return func(msg *message.Message) ([]*message.Message, error) {
				ctx := msg.Context()

				// check retry
				subCtx, isRetry := ctx.Value(ctxKey(packName)).(context.Context)
				if !isRetry {
					// first try
					subCtx = ctx
				} else {
					ctx = subCtx
				}

				sp, newCtx := opentracing.StartSpanFromContext(ctx, title+": "+message.HandlerNameFromCtx(ctx))
				defer sp.Finish()

				// set tags
				sp.SetTag("ctx.handlerName", message.HandlerNameFromCtx(ctx)).
					SetTag("ctx.publisherName", message.PublisherNameFromCtx(ctx)).
					SetTag("ctx.publishTopic", message.PublishTopicFromCtx(ctx)).
					SetTag("ctx.subscriberName", message.SubscriberNameFromCtx(ctx)).
					SetTag("ctx.subscribeTopic", message.SubscribeTopicFromCtx(ctx)).
					SetTag("watermill.middleware", packName)
				ext.Component.Set(sp, "watermill_wrap_middleware")

				// update ctx
				ctx = context.WithValue(newCtx, ctxKey(packName), subCtx)
				msg.SetContext(ctx)

				// next middleware
				r, err := h(msg)
				if err != nil {
					ext.LogError(sp, err)
				}
				return r, err
			}
		}(m(h))
	}
}

type handler struct{}

func (handler) Middleware(h message.HandlerFunc) message.HandlerFunc {
	return h
}
