package watermilltracing

import (
	"context"

	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/opentracing/opentracing-go"
)

func NewMessage(ctx context.Context, uuid string, payload message.Payload) *message.Message {
	msg := message.NewMessage(uuid, payload)
	opentracing.GlobalTracer().Inject(
		opentracing.SpanFromContext(ctx).Context(),
		opentracing.TextMap,
		opentracing.TextMapCarrier(msg.Metadata),
	)
	msg.SetContext(ctx)
	return msg
}
