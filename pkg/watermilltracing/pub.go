package watermilltracing

import (
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

func WrapPub(p message.Publisher) message.Publisher {
	return &publisher{Publisher: p}
}

type publisher struct {
	message.Publisher
}

func (p *publisher) Publish(topic string, messages ...*message.Message) error {
	for _, msg := range messages {
		ctx := msg.Context()
		sp, _ := opentracing.StartSpanFromContext(ctx, "pub: "+topic)
		ext.SpanKind.Set(sp, ext.SpanKindProducerEnum)
		sp.SetTag("msg.UUID", msg.UUID)
		for k, v := range msg.Metadata {
			sp.SetTag("msg.Metadata."+k, v)
		}
		sp.SetTag("msg.Payload", string(msg.Payload))
		if err := p.Publisher.Publish(topic, msg); err != nil {
			return err
		}
		sp.Finish()
	}
	return nil
}
