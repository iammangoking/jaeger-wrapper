package httptracing

import (
	"net/http"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.com/iammangoking/jaeger-wrapper/internel/tracer"
)

func Do(req *http.Request) (*http.Response, error) {
	sp, _ := opentracing.StartSpanFromContext(req.Context(), "sent: "+req.URL.Path)
	defer sp.Finish()
	ext.PeerHostname.Set(sp, req.Host)
	ext.HTTPUrl.Set(sp, req.URL.Path)
	ext.HTTPMethod.Set(sp, req.Method)
	ext.Component.Set(sp, "http_client")
	req = req.WithContext(opentracing.ContextWithSpan(req.Context(), sp))

	if err := tracer.InjectHeader(req.Context(), req.Header); err != nil {
		return nil, err
	}
	return http.DefaultClient.Do(req)
}

func DoWithClient(client http.RoundTripper, req *http.Request) (*http.Response, error) {
	sp, _ := opentracing.StartSpanFromContext(req.Context(), "sent: "+req.URL.Path)
	defer sp.Finish()
	ext.PeerHostname.Set(sp, req.Host)
	ext.HTTPUrl.Set(sp, req.URL.Path)
	ext.HTTPMethod.Set(sp, req.Method)
	ext.Component.Set(sp, "http_client")
	req = req.WithContext(opentracing.ContextWithSpan(req.Context(), sp))

	if err := tracer.InjectHeader(req.Context(), req.Header); err != nil {
		return nil, err
	}
	return client.RoundTrip(req)
}

func MiddlewareFunc(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// 使用 opentracing.GlobalTracer() 获取全局 Tracer
		// tracer := opentracing.GlobalTracer()
		// spCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
		spCtx, _ := tracer.ExtractHeader(r.Header)
		// 生成依赖关系，并新建一个 span、
		// 这里很重要，因为生成了  References []SpanReference 依赖关系
		sp := opentracing.GlobalTracer().StartSpan("recv: "+r.URL.Path, ext.RPCServerOption(spCtx))
		defer sp.Finish()

		// 记录 tag
		// 记录请求 Url
		ext.HTTPUrl.Set(sp, r.URL.Path)
		// Http Method
		ext.HTTPMethod.Set(sp, r.Method)
		// 记录组件名称
		ext.Component.Set(sp, "http_middlewareFunc")

		// 在 header 中加上当前进程的上下文信息
		r = r.WithContext(opentracing.ContextWithSpan(r.Context(), sp))

		next(w, r)
	}
}

func Middleware(next http.Handler) http.Handler {
	return MiddlewareFunc(next.ServeHTTP)
}
