package gqltracing

import (
	"context"
	"fmt"

	"github.com/99designs/gqlgen-contrib/gqlopentracing"
	"github.com/99designs/gqlgen/graphql"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
)

type Tracer struct {
	gqlopentracing.Tracer
}

func (a Tracer) InterceptOperation(ctx context.Context, next graphql.OperationHandler) graphql.ResponseHandler {
	oc := graphql.GetOperationContext(ctx)
	span, ctx := opentracing.StartSpanFromContext(ctx, oc.Operation.Name)
	// span := opentracing.SpanFromContext(ctx)
	span.SetTag("resolver.rawQuery", oc.RawQuery)
	ext.SpanKind.Set(span, "server")
	ext.Component.Set(span, "gqlgen")
	defer span.Finish()

	return next(ctx)
}

func (a Tracer) InterceptField(ctx context.Context, next graphql.Resolver) (interface{}, error) {
	fc := graphql.GetFieldContext(ctx)
	// span := opentracing.SpanFromContext(ctx)
	span, ctx := opentracing.StartSpanFromContext(ctx, fc.Object+"_"+fc.Field.Name)
	span.SetOperationName(fc.Object + "_" + fc.Field.Name)
	span.SetTag("resolver.object", fc.Object)
	span.SetTag("resolver.field", fc.Field.Name)
	defer span.Finish()

	res, err := next(ctx)

	errList := graphql.GetFieldErrors(ctx, fc)
	if len(errList) != 0 {
		ext.Error.Set(span, true)
		span.LogFields(
			log.String("event", "error"),
		)

		for idx, err := range errList {
			span.LogFields(
				log.String(fmt.Sprintf("error.%d.message", idx), err.Error()),
				log.String(fmt.Sprintf("error.%d.kind", idx), fmt.Sprintf("%T", err)),
			)
		}
	}

	return res, err
}
