package gormv2tracing

import (
	"gorm.io/gorm"
	gormopentracing "gorm.io/plugin/opentracing"
)

func Plugin() gorm.Plugin {
	return gormopentracing.New()
}
