package grpctracing

import (
	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	"github.com/opentracing/opentracing-go"
	"google.golang.org/grpc"
)

type Option = otgrpc.Option

func LogPayloads() Option {
	return otgrpc.LogPayloads()
}

type SpanInclusionFunc = otgrpc.SpanInclusionFunc

func IncludingSpans(inclusionFunc SpanInclusionFunc) Option {
	return otgrpc.IncludingSpans(inclusionFunc)
}

type SpanDecoratorFunc = otgrpc.SpanDecoratorFunc

func SpanDecorator(decorator SpanDecoratorFunc) Option {
	return otgrpc.SpanDecorator(decorator)
}

func OpenTracingServerInterceptor(optFuncs ...Option) grpc.UnaryServerInterceptor {
	return otgrpc.OpenTracingServerInterceptor(opentracing.GlobalTracer(), optFuncs...)
}

func OpenTracingClientInterceptor(optFuncs ...Option) grpc.UnaryClientInterceptor {
	return otgrpc.OpenTracingClientInterceptor(opentracing.GlobalTracer(), optFuncs...)
}

func OpenTracingStreamServerServerInterceptor(optFuncs ...Option) grpc.StreamServerInterceptor {
	return otgrpc.OpenTracingStreamServerInterceptor(opentracing.GlobalTracer(), optFuncs...)
}

func OpenTracingStreamServerClientInterceptor(optFuncs ...Option) grpc.StreamClientInterceptor {
	return otgrpc.OpenTracingStreamClientInterceptor(opentracing.GlobalTracer(), optFuncs...)
}
