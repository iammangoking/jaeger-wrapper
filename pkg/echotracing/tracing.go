package echotracing

import (
	"github.com/labstack/echo/v4"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.com/iammangoking/jaeger-wrapper/internel/tracer"
)

func MiddlewareFunc() echo.MiddlewareFunc {
	handler := func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			// 使用 opentracing.GlobalTracer() 获取全局 Tracer
			// tracer := opentracing.GlobalTracer()
			// spCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(c.Request().Header))
			spCtx, _ := tracer.ExtractHeader(c.Request().Header)
			// 生成依赖关系，并新建一个 span、
			// 这里很重要，因为生成了  References []SpanReference 依赖关系
			sp := opentracing.GlobalTracer().StartSpan("recv: "+c.Request().URL.Path, ext.RPCServerOption(spCtx))
			defer sp.Finish()

			// 记录 tag
			// 记录请求 Url
			ext.HTTPUrl.Set(sp, c.Request().URL.Path)
			// Http Method
			ext.HTTPMethod.Set(sp, c.Request().Method)
			// 记录组件名称
			ext.Component.Set(sp, "echo_middlewareFunc")

			// 在 header 中加上当前进程的上下文信息
			c.SetRequest(c.Request().WithContext(opentracing.ContextWithSpan(c.Request().Context(), sp)))

			// 继续设置 tag
			defer ext.HTTPStatusCode.Set(sp, uint16(c.Response().Status))

			// 传递给下一个中间件
			return next(c)
		}
	}

	return handler
}
