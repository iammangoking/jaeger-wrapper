package redistracing

import (
	"github.com/go-redis/redis/v8"
	opentracing_redis "github.com/laststem/go-opentracing-redis"
	"github.com/opentracing/opentracing-go"
)

func Wrap(c *redis.Client) {
	opentracing_redis.Customize = func(span opentracing.Span, cmder redis.Cmder) {
		span.LogKV("cmd", cmder.String())
	}
	opentracing_redis.Wrap(c)
}
