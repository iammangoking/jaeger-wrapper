package gormv1tracing

import (
	"context"
	"database/sql/driver"
	"fmt"
	"reflect"
	"regexp"
	"runtime"
	"time"
	"unicode"

	"github.com/jinzhu/gorm"
	"github.com/opentracing/opentracing-go"
	"go.opentelemetry.io/otel/codes"
)

//Gorm scope keys for passing around context and span within the DB scope
var (
	contextScopeKey = "_otContext"
	spanScopeKey    = "_otSpan"
)

// WithContext sets the current context in the db instance for instrumentation.
func WithContext(ctx context.Context, db *gorm.DB) *gorm.DB {
	return db.Set(contextScopeKey, ctx)
}

// RegisterCallbacks registers the necessary callbacks in Gorm's hook system for instrumentation with OpenTelemetry Spans.
func RegisterCallbacks(db *gorm.DB) {
	c := &callbacks{
		allowRoot: true,
		//	defaultAttributes: []attribute.KeyValue{},
	}
	//defaultOpts := []Option{
	//	// Default to the global tracer if not configured
	//	WithTracer(otel.GetTracerProvider().Tracer("otgorm")),
	//	WithSpanOptions(trace.WithSpanKind(trace.SpanKindInternal)),
	//}

	//for _, opt := range append(defaultOpts, opts...) {
	//	opt.apply(c)
	//}

	db.Callback().Create().Before("gorm:create").Register("before_create", c.beforeCreate)
	db.Callback().Create().After("gorm:create").Register("after_create", c.afterCreate)
	db.Callback().Query().Before("gorm:query").Register("before_query", c.beforeQuery)
	db.Callback().Query().After("gorm:query").Register("after_query", c.afterQuery)
	db.Callback().Update().Before("gorm:update").Register("before_update", c.beforeUpdate)
	db.Callback().Update().After("gorm:update").Register("after_update", c.afterUpdate)
	db.Callback().Delete().Before("gorm:delete").Register("before_delete", c.beforeDelete)
	db.Callback().Delete().After("gorm:delete").Register("after_delete", c.afterDelete)
	db.Callback().RowQuery().Before("gorm:row_query").Register("before_row_query", c.beforeRowQuery)
	db.Callback().RowQuery().After("gorm:row_query").Register("after_row_query", c.afterRowQuery)
}

type callbacks struct {
	//Allow otgorm to create root spans in the absence of a parent span.
	//Default is to not allow root spans.
	allowRoot bool
}

func (c *callbacks) before(scope *gorm.Scope, operation string) {
	rctx, _ := scope.Get(contextScopeKey)
	ctx, ok := rctx.(context.Context)
	if !ok || ctx == nil {
		ctx = context.Background()
	}

	ctx = c.startTrace(ctx, scope, operation)

	scope.Set(contextScopeKey, ctx)
}

func (c *callbacks) after(scope *gorm.Scope) {
	c.endTrace(scope)
}

func (c *callbacks) startTrace(ctx context.Context, scope *gorm.Scope, operation string) context.Context {
	// There's no context but we are ok with root spans
	if ctx == nil {
		ctx = context.Background()
	}

	var span opentracing.Span
	//If there's no parent span and we don't allow root spans, return context
	span, _ = opentracing.StartSpanFromContext(ctx, fmt.Sprintf("gorm:%s", operation))
	if !c.allowRoot {
		return ctx
	}

	scope.Set(spanScopeKey, span)

	return ctx
}

func (c *callbacks) endTrace(scope *gorm.Scope) {
	rspan, ok := scope.Get(spanScopeKey)
	if !ok {
		return
	}

	span, ok := rspan.(opentracing.Span)
	if !ok {
		return
	}

	span.SetTag("gorm.table", scope.TableName)
	span.SetTag("gorm.query", LogFormatter(scope.SQL, scope.SQLVars))

	span.SetTag("path", fileWithLineNum())

	//Set StatusCode if there are any issues
	code := codes.Ok
	msg := ""
	if scope.HasError() {
		err := scope.DB().Error
		code = codes.Error
		if gorm.IsRecordNotFoundError(err) {
			msg = "gorm:NotFound"
		} else {
			msg = "gorm:Unknown"
		}

	}

	span.LogKV(code, msg)

	//End Span
	span.Finish()
}

func (c *callbacks) beforeCreate(scope *gorm.Scope) {
	c.before(scope, "create")
}
func (c *callbacks) afterCreate(scope *gorm.Scope)    { c.after(scope) }
func (c *callbacks) beforeQuery(scope *gorm.Scope)    { c.before(scope, "query") }
func (c *callbacks) afterQuery(scope *gorm.Scope)     { c.after(scope) }
func (c *callbacks) beforeUpdate(scope *gorm.Scope)   { c.before(scope, "update") }
func (c *callbacks) afterUpdate(scope *gorm.Scope)    { c.after(scope) }
func (c *callbacks) beforeDelete(scope *gorm.Scope)   { c.before(scope, "delete") }
func (c *callbacks) afterDelete(scope *gorm.Scope)    { c.after(scope) }
func (c *callbacks) beforeRowQuery(scope *gorm.Scope) { c.before(scope, "row_query") }
func (c *callbacks) afterRowQuery(scope *gorm.Scope)  { c.after(scope) }

func fileWithLineNum() string {
	_, file, line, ok := runtime.Caller(6)
	if ok {
		return fmt.Sprintf("%v:%v", file, line)
	}
	return ""
}

var (
	sqlRegexp                = regexp.MustCompile(`\?`)
	numericPlaceHolderRegexp = regexp.MustCompile(`\$\d+`)
	goSrcRegexp              = regexp.MustCompile(`golang/ocgorm(@.*)?/.*.go`)
	goTestRegexp             = regexp.MustCompile(`jinzhu/gorm(@.*)?/.*test.go`)
)

func isPrintable(s string) bool {
	for _, r := range s {
		if !unicode.IsPrint(r) {
			return false
		}
	}
	return true
}

var LogFormatter = func(values ...interface{}) string {
	var (
		sql             string
		formattedValues []string
	)

	for _, value := range values[1].([]interface{}) {
		indirectValue := reflect.Indirect(reflect.ValueOf(value))
		if indirectValue.IsValid() {
			value = indirectValue.Interface()
			if t, ok := value.(time.Time); ok {
				if t.IsZero() {
					formattedValues = append(formattedValues, fmt.Sprintf("'%v'", "0000-00-00 00:00:00"))
				} else {
					formattedValues = append(formattedValues, fmt.Sprintf("'%v'", t.Format("2006-01-02 15:04:05")))
				}
			} else if b, ok := value.([]byte); ok {
				if str := string(b); isPrintable(str) {
					formattedValues = append(formattedValues, fmt.Sprintf("'%v'", str))
				} else {
					formattedValues = append(formattedValues, "'<binary>'")
				}
			} else if r, ok := value.(driver.Valuer); ok {
				if value, err := r.Value(); err == nil && value != nil {
					formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
				} else {
					formattedValues = append(formattedValues, "NULL")
				}
			} else {
				switch value.(type) {
				case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, float32, float64, bool:
					formattedValues = append(formattedValues, fmt.Sprintf("%v", value))
				default:
					formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
				}
			}
		} else {
			formattedValues = append(formattedValues, "NULL")
		}
	}

	// differentiate between $n placeholders or else treat like ?
	if numericPlaceHolderRegexp.MatchString(values[0].(string)) {
		sql = values[0].(string)
		for index, value := range formattedValues {
			placeholder := fmt.Sprintf(`\$%d([^\d]|$)`, index+1)
			sql = regexp.MustCompile(placeholder).ReplaceAllString(sql, value+"$1")
		}
	} else {
		formattedValuesLength := len(formattedValues)
		for index, value := range sqlRegexp.Split(values[0].(string), -1) {
			sql += value
			if index < formattedValuesLength {
				sql += formattedValues[index]
			}
		}
	}

	return sql
}
