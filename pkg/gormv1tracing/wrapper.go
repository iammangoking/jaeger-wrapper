package gormv1tracing

import (
	"context"

	"github.com/jinzhu/gorm"
)

type DB interface {
	Write(context.Context) *gorm.DB
	Read(context.Context) *gorm.DB
}

func Wrap(writer *gorm.DB, reader *gorm.DB) DB {
	RegisterCallbacks(writer)
	if writer != reader {
		RegisterCallbacks(reader)
	}
	return &_db{w: writer, r: reader}
}

type _db struct {
	w *gorm.DB
	r *gorm.DB
}

func (db *_db) Write(ctx context.Context) *gorm.DB {
	return WithContext(ctx, db.w)
}

func (db *_db) Read(ctx context.Context) *gorm.DB {
	return WithContext(ctx, db.w)
}
