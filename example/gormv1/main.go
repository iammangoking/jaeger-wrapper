package main

import (
	"context"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gormv1tracing"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func main() {
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	sp := opentracing.StartSpan("example")
	defer sp.Finish()

	ctx = opentracing.ContextWithSpan(ctx, sp)

	db = gormv1tracing.WithContext(ctx, db)
	gormv1tracing.RegisterCallbacks(db)

	db.Debug().AutoMigrate(&Product{})
	db.Debug().Create(&Product{Code: "D42", Price: 100})

	// Create
	db.Debug().Create(&Product{Code: "D42", Price: 100})
	// Read
	var product Product
	db.Debug().First(&product, 1)
	db.Debug().First(&product, "code = ?", "D42")
	// Update
	db.Debug().Model(&product).Update("Price", 200)
	// Update
	db.Debug().Model(&product).Updates(Product{Price: 200, Code: "F42"})
	db.Debug().Model(&product).Updates(map[string]interface{}{"Price": 200, "Code": "F42"})
	//
	// Delete
	db.Debug().Delete(&product, 1)

	tx := db.Debug().Begin()
	tx.Debug().First(&product, 1)
	tx.Debug().Commit()

	//db.Debug().Transaction(func(tx *gorm.DB) error {
	//	db.Debug().Set("gorm:query_option", "FOR UPDATE").First(&product, 1)
	//
	//	db.Debug().Set("gorm:query_option", "SHARD MODE").First(&product, 1)
	//	return nil
	//})

}
