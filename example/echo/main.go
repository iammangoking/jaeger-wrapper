package main

import (
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/echotracing"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
)

func main() {
	// init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	// sub handler
	go CreateHttpServer(":7070", func(ctx echo.Context) error {
		return ctx.String(http.StatusOK, "pong! "+xid.New().String())
	})
	// main handler
	CreateHttpServer(":8080", func(ctx echo.Context) error {
		req, err := http.NewRequestWithContext(ctx.Request().Context(), "GET", "http://localhost:7070/ping", nil)
		if err != nil {
			return ctx.String(http.StatusInternalServerError, err.Error())
		}
		resp, err := httptracing.Do(req)
		if err != nil {
			return ctx.String(http.StatusInternalServerError, err.Error())
		}
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		if err != nil {
			return ctx.String(http.StatusInternalServerError, err.Error())
		}
		bodyString := string(bodyBytes)

		return ctx.String(resp.StatusCode, bodyString)
	})
}

func CreateHttpServer(addr string, handler func(echo.Context) error) {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(echotracing.MiddlewareFunc())

	// Routes
	e.GET("/ping", handler)

	// Start server
	e.Logger.Fatal(e.Start(addr))
}
