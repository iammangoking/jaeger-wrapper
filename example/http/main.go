package main

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gintracing"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
)

func main() {
	// init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	r := gin.Default()
	r.Use(gintracing.MiddlewareFunc())
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	go r.Run(":8080")

	transport := &http.Transport{
		MaxConnsPerHost:     24,
		MaxIdleConnsPerHost: 24,
		MaxIdleConns:        48,
		IdleConnTimeout:     60 * time.Second,
	}

	req, err := http.NewRequest(http.MethodGet, "http://127.0.0.1:8080/ping", nil)
	if err != nil {
		panic(err)
	}
	//case 1 without root span
	_, err = httptracing.DoWithClient(transport, req)

	//case 2 with root span
	tracer := opentracing.GlobalTracer()
	sp := tracer.StartSpan("test")
	sp.Finish()
	ct := opentracing.ContextWithSpan(context.Background(), sp)
	req = req.WithContext(ct)

	_, err = httptracing.DoWithClient(transport, req)
	//_, err = httptracing.Do(ct, req)//use default transport

	if err != nil {
		panic(err)
	}
}
