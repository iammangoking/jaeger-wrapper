package main

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/example/grpc/hello"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/grpctracing"
	"google.golang.org/grpc"
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	hello.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(ctx context.Context, in *hello.HelloRequest) (*hello.HelloReply, error) {
	return &hello.HelloReply{Message: "Hello " + in.GetName()}, nil
}
func (s *server) StreamingInputCall(in hello.Greeter_StreamingInputCallServer) error {
	var name string
	for {
		in, err := in.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		name = in.Name
	}
	return in.SendAndClose(&hello.HelloReply{Message: name})
}

func (s *server) StreamingBidirectionalCall(stream hello.Greeter_StreamingBidirectionalCallServer) error {
	for {
		in, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		if err = stream.SendMsg(&hello.HelloReply{Message: in.Name}); err != nil {
			return err
		}
	}
}

const (
	port        = ":50051"
	address     = "localhost:50051"
	defaultName = "world"
)

func main() {
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	//server
	lis, err := net.Listen("tcp", port)
	if err != nil {
		panic(fmt.Errorf("failed to listen: %v", err))
	}
	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			grpctracing.OpenTracingServerInterceptor(grpctracing.LogPayloads()),
		),
		grpc.StreamInterceptor(
			grpctracing.OpenTracingStreamServerServerInterceptor(grpctracing.LogPayloads()),
		),
	)

	hello.RegisterGreeterServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	go func() {
		if err := s.Serve(lis); err != nil {
			panic(fmt.Errorf("failed to serve: %v", err))
		}
	}()
	time.Sleep(time.Second)

	//client
	// Set up a connection to the server.
	conn, err := grpc.Dial(
		address, grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(grpctracing.OpenTracingClientInterceptor(grpctracing.LogPayloads())),
		grpc.WithStreamInterceptor(
			grpctracing.OpenTracingStreamServerClientInterceptor(grpctracing.LogPayloads()),
		),
		grpc.WithBlock())
	if err != nil {
		panic(fmt.Errorf("did not connect: %v", err))
	}
	defer conn.Close()
	c := hello.NewGreeterClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.SayHello(ctx, &hello.HelloRequest{Name: name})
	if err != nil {
		panic(fmt.Errorf("could not greet: %v", err))
	}
	log.Printf("Greeting: %s", r.GetMessage())

	streamLength := 5

	//streaming msg reply common
	stream, err := c.StreamingInputCall(context.Background())
	for i := 0; i < streamLength; i++ {
		if err = stream.Send(&hello.HelloRequest{Name: name}); err != nil {
			panic(fmt.Errorf("Failed StreamingInputCall: %v", err))
		}
	}
	resp, err := stream.CloseAndRecv()
	if err != nil {
		panic(fmt.Errorf("Failed StreamingInputCall: %v", err))
	}
	log.Log().Msg(resp.Message)

	//streaming msg reply streaming
	streamB, err := c.StreamingBidirectionalCall(context.Background())
	if err != nil {
		panic(fmt.Errorf("Failed StreamingInputCall: %v", err))
	}

	go func() {
		for i := 0; i < streamLength; i++ {
			if err := streamB.Send(&hello.HelloRequest{Name: name}); err != nil {
				panic(fmt.Errorf("Failed StreamingInputCall: %v", err))
			}
		}
		streamB.CloseSend()
	}()
	for {
		resp, err := streamB.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(fmt.Errorf("Failed StreamingOutputCall: %v", err))
		}
		log.Log().Msg(resp.Message)
	}
}
