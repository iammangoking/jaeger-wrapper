# complex example

## usage

% complex [`agent` / `server` / `worker`]

then request `http://localhost:8080/hello`, success when response with `world!`

## cmd

* agent: To transport request to server

* server: Handle the business logic

* worker: Slow handler to handle queue

## workflow

1. agent get request from `/hello`
    * with id and auth

1. agent request `server`
    * with id and auth

1. server get data from `DB`
    * user data

1. server set data to `redis`
    * otp: id

1. server response to `agent`

1. agent get data from `redis`
    * id

1. agent grpc call to `server`
    * with id and otp

1. server get data from `redis`
    * id

1. server set data to `DB`
    * order

1. server `pub` msg to queue
    * order id

1. server response to `agent`
    * order status

1. agent response `world!`

1. worker `sub` msg from queue

1. worker update data to `DB`
