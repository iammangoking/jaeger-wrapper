package provider

import (
	"fmt"

	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/proto"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/grpctracing"
	"google.golang.org/grpc"
)

func NewGrpcClient(addr string) (proto.OrderClient, *grpc.ClientConn, error) {
	conn, err := grpc.Dial(
		addr,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(grpctracing.OpenTracingClientInterceptor(grpctracing.LogPayloads())),
		grpc.WithStreamInterceptor(grpctracing.OpenTracingStreamServerClientInterceptor(grpctracing.LogPayloads())),
		grpc.WithBlock())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return proto.NewOrderClient(conn), conn, nil
}

func NewGrpcServer(srv proto.OrderServer) *grpc.Server {
	s := grpc.NewServer(
		grpc.UnaryInterceptor(grpctracing.OpenTracingServerInterceptor(grpctracing.LogPayloads())),
		grpc.StreamInterceptor(grpctracing.OpenTracingStreamServerServerInterceptor(grpctracing.LogPayloads())),
	)
	proto.RegisterOrderServer(s, srv)
	return s
}
