package provider

import (
	"context"

	"github.com/gin-gonic/gin"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gintracing"
)

func NewGin(ctx context.Context) *gin.Engine {
	r := gin.Default()

	r.Use(gintracing.MiddlewareFunc())

	return r
}
