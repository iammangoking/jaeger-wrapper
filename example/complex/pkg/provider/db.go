package provider

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/model"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gormv2tracing"
)

func NewDB(ctx context.Context) *gorm.DB {
	dsn := "root:123456@tcp(localhost:3306)/jaeger_complex_test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.Use(gormv2tracing.Plugin())

	sp := opentracing.StartSpan("gormv2 init")
	defer sp.Finish()
	init := db.WithContext(opentracing.ContextWithSpan(ctx, sp))

	err = init.Debug().AutoMigrate(&model.User{})
	if err != nil {
		panic(err)
	}
	err = init.Debug().AutoMigrate(&model.Order{})
	if err != nil {
		panic(err)
	}
	init.FirstOrCreate(&model.User{}, model.User{}.SetUserId("aaaaa").SetToken("11111"))

	return db.WithContext(ctx)
}
