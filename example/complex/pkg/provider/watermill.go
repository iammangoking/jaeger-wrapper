package provider

import (
	"context"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/ThreeDotsLabs/watermill/message/router/plugin"

	"gitlab.com/iammangoking/jaeger-wrapper/pkg/watermilltracing"
)

var (
	WMlogger = watermill.NewStdLogger(false, false)

	Topic = "complex_worker_topic"
)

func NewRouter(ctx context.Context) (*message.Router, error) {
	router, err := message.NewRouter(message.RouterConfig{}, WMlogger)
	if err != nil {
		return nil, err
	}

	router.AddPlugin(plugin.SignalsHandler)

	router.AddMiddleware(
		// wrapper for middlewares to collect error
		watermilltracing.WrapMiddlewares(
			middleware.CorrelationID,

			// StartSpan when doing retry
			watermilltracing.TraceWithSpan(
				middleware.Retry{
					MaxRetries:      3,
					InitialInterval: time.Millisecond * 100,
					Logger:          WMlogger,
				}.Middleware,
			),

			middleware.Recoverer,

			// Test retry
			watermilltracing.TraceWithSpan(middleware.RandomFail(0.7)),
		)...,
	)

	return router, nil
}

func NewPuber(ctx context.Context) message.Publisher {
	amqpURI := "amqp://user:bitnami@localhost:5672/"
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)
	var r message.Publisher
	r, _ = amqp.NewPublisher(amqpConfig, WMlogger)
	// tracing wrapper
	return watermilltracing.WrapPub(r)
}

func NewSuber(ctx context.Context) message.Subscriber {
	amqpURI := "amqp://user:bitnami@localhost:5672/"
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)
	var r message.Subscriber
	r, _ = amqp.NewSubscriber(amqpConfig, WMlogger)
	// tracing wrapper
	return watermilltracing.WrapSub(r)
}
