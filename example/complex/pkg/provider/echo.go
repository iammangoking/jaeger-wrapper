package provider

import (
	"context"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/iammangoking/jaeger-wrapper/pkg/echotracing"
)

func NewEcho(ctx context.Context) *echo.Echo {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(echotracing.MiddlewareFunc())

	return e
}
