package provider

import (
	"context"

	"github.com/go-redis/redis/v8"

	"gitlab.com/iammangoking/jaeger-wrapper/pkg/redistracing"
)

func NewRedisClient(ctx context.Context) *redis.Client {
	// init redis
	client := redis.NewClient(&redis.Options{})

	// wrap redis
	redistracing.Wrap(client)

	return client
}
