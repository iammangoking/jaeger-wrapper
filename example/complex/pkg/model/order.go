package model

import "gorm.io/gorm"

const (
	Status_pending    = "pending"
	Status_processing = "processing"
	Status_fin        = "fin"
)

type Order struct {
	gorm.Model

	UserId  *string `gorm:"user_id"`
	OrderId *string `gorm:"order_id"`
	Status  *string `gorm:"status"`
}

func (o Order) SetUserId(i string) *Order {
	o.UserId = &i
	return &o
}

func (o Order) SetOrderId(i string) *Order {
	o.OrderId = &i
	return &o
}

func (o Order) SetStatus(i string) *Order {
	o.Status = &i
	return &o
}
