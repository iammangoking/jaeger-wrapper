package model

import "gorm.io/gorm"

type User struct {
	gorm.Model

	UserId *string `gorm:"user_id"`
	Token  *string `gorm:"token"`
}

func (u User) SetUserId(i string) *User {
	u.UserId = &i
	return &u
}

func (u User) SetToken(i string) *User {
	u.Token = &i
	return &u
}
