package model

type Auth struct {
	Id    string `json:"id"`
	Token string `json:"t"`
}

type AuthPayload struct {
	Otp string
}

type WorkerPayload struct {
	OrderId string
}
