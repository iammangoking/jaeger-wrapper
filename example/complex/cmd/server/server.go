package server

import (
	"context"
	"encoding/json"
	"errors"
	"net"
	"net/http"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gorm.io/gorm"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/model"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/proto"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/provider"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/watermilltracing"
)

var rc *redis.Client
var db *gorm.DB
var pub message.Publisher

var Cmd = &cobra.Command{
	Use: "server",
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		// init
		jaeger.InitClient(example.Config(), &log.Logger)
		defer jaeger.Close()

		// redis
		rc = provider.NewRedisClient(ctx)

		// db
		db = provider.NewDB(ctx)

		// pub
		pub = provider.NewPuber(ctx)
		defer pub.Close()

		// gin handler
		e := provider.NewGin(ctx)
		e.GET("/auth", auth)

		// grpc server
		s := provider.NewGrpcServer(&orderServer{})
		lis, err := net.Listen("tcp", ":6060")
		if err != nil {
			return
		}
		go func() {
			log.Err(s.Serve(lis))
		}()

		// Start server
		log.Err(e.Run(":7070"))
	},
}

func auth(ctx *gin.Context) {
	// 解包
	a := model.Auth{}
	if err := ctx.BindJSON(&a); err != nil {
		ctx.String(http.StatusBadRequest, err.Error())
		return
	}
	// auth
	db := db.WithContext(ctx.Request.Context())
	if err := db.First(model.User{}.SetUserId(a.Id).SetToken(a.Token)).Error; err != nil {
		ctx.String(http.StatusBadRequest, err.Error())
		return
	}
	otp := xid.New().String()

	// otp
	rc.Set(ctx.Request.Context(), otp, a.Id, time.Second*5)

	ctx.JSON(http.StatusOK, model.AuthPayload{Otp: otp})
}

type orderServer struct {
	proto.OrderServer
}

func (o orderServer) Create(ctx context.Context, req *proto.CreateReq) (*proto.CreateResp, error) {
	uid := rc.Get(ctx, req.Otp).Val()
	if uid != req.User {
		return nil, errors.New("user error")
	}

	orderId := xid.New().String()

	// db create
	db := db.WithContext(ctx)
	if err := db.Create(model.Order{}.SetUserId(uid).SetOrderId(orderId).SetStatus(model.Status_pending)).Error; err != nil {
		return nil, err
	}

	// pub to worker
	mp, _ := json.Marshal(model.WorkerPayload{OrderId: orderId})
	if err := pub.Publish(provider.Topic, watermilltracing.NewMessage(ctx, watermill.NewUUID(), mp)); err != nil {
		return nil, err
	}

	return &proto.CreateResp{Status: "ok"}, nil
}
