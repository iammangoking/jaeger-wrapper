package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/cmd/agent"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/cmd/server"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/cmd/worker"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use: "complex",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) {},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.AddCommand(
		agent.Cmd,
		server.Cmd,
		worker.Cmd,
	)
	cobra.CheckErr(rootCmd.Execute())
}
