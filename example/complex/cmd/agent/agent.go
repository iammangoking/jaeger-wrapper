package agent

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/model"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/proto"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/provider"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
)

var Cmd = &cobra.Command{
	Use: "agent",
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		// init
		jaeger.InitClient(example.Config(), &log.Logger)
		defer jaeger.Close()

		// grpc client
		grpc, conn, err := provider.NewGrpcClient("localhost:6060")
		if err != nil {
			return
		}
		defer conn.Close()

		// echo handler
		e := provider.NewEcho(ctx)
		e.POST("/hello", func(c echo.Context) error {
			a := model.Auth{}
			ap := model.AuthPayload{}
			reqBody := []byte{}
			// copy body
			if c.Request().Body != nil { // Read
				reqBody, _ = ioutil.ReadAll(c.Request().Body)
			}
			c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(reqBody)) // Reset
			// check
			{
				if err := c.Bind(&a); err != nil {
					return c.String(http.StatusBadRequest, err.Error())
				}
			}
			// auth
			{

				req, err := http.NewRequest(http.MethodGet, "http://localhost:7070/auth", bytes.NewBuffer(reqBody))
				if err != nil {
					return c.String(http.StatusInternalServerError, err.Error())
				}
				req = req.WithContext(c.Request().Context())
				resp, err := httptracing.Do(req)
				if err != nil {
					return c.String(http.StatusInternalServerError, err.Error())
				}
				respB, _ := ioutil.ReadAll(resp.Body)
				defer resp.Body.Close()
				if err := json.Unmarshal(respB, &ap); err != nil {
					return c.String(http.StatusInternalServerError, err.Error())
				}
			}

			// order
			if _, err := grpc.Create(
				c.Request().Context(),
				&proto.CreateReq{
					User: a.Id,
					Otp:  ap.Otp,
				},
			); err != nil {
				return c.String(http.StatusInternalServerError, err.Error())
			}

			return c.String(http.StatusOK, "world!")
		})

		// Start server
		e.Logger.Fatal(e.Start(":8080"))
	},
}
