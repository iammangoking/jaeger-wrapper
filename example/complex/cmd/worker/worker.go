package worker

import (
	"context"
	"encoding/json"

	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/model"
	"gitlab.com/iammangoking/jaeger-wrapper/example/complex/pkg/provider"
)

var Cmd = &cobra.Command{
	Use: "worker",
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		// Jaeger init
		jaeger.InitClient(example.Config(), &log.Logger)
		defer jaeger.Close()

		// db
		db := provider.NewDB(ctx)

		// amqp pubsub
		sub := provider.NewSuber(ctx)
		defer sub.Close()

		router, err := provider.NewRouter(ctx)
		if err != nil {
			panic(err)
		}

		router.AddNoPublisherHandler(
			"worker_handle_messages",
			provider.Topic,
			sub,
			func(msg *message.Message) error {
				// 先解包
				p := model.WorkerPayload{}
				if err := json.Unmarshal(msg.Payload, &p); err != nil {
					return err
				}
				// 改DB
				db := db.WithContext(msg.Context())
				if err := db.Where(model.Order{}.SetOrderId(p.OrderId)).
					Updates(model.Order{}.SetStatus(model.Status_fin)).Error; err != nil {
					log.Err(err).Msg("worker db update err")
					return err
				}
				return nil
			},
		)

		if err := router.Run(context.Background()); err != nil {
			panic(err)
		}
	},
}
