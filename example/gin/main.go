package main

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
)

func main() {
	// init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	r.Use()
	go r.Run(":8080")
	transport := &http.Transport{
		MaxConnsPerHost:     24,
		MaxIdleConnsPerHost: 24,
		MaxIdleConns:        48,
		IdleConnTimeout:     60 * time.Second,
	}

	req, _ := http.NewRequestWithContext(context.Background(), "Get", "https://google.com", nil)
	_, err := httptracing.DoWithClient(transport, req)
	if err != nil {
		panic(err)
	}
	time.Sleep(time.Second * 5)
}
