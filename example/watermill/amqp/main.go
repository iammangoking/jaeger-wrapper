package main

import (
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"

	watermillexample "gitlab.com/iammangoking/jaeger-wrapper/example/watermill"
)

func main() {
	suber, err := Sub()
	if err != nil {
		panic(err)
	}
	puber, err := Pub()
	if err != nil {
		panic(err)
	}

	watermillexample.Run(suber, puber)
}

func Sub() (message.Subscriber, error) {
	amqpURI := "amqp://user:bitnami@localhost:5672/"
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)

	return amqp.NewSubscriber(
		// This config is based on this example: https://www.rabbitmq.com/tutorials/tutorial-two-go.html
		// It works as a simple queue.
		//
		// If you want to implement a Pub/Sub style service instead, check
		// https://watermill.io/pubsubs/amqp/#amqp-consumer-groups
		amqpConfig,
		watermill.NewStdLogger(false, false),
	)
}

func Pub() (message.Publisher, error) {
	amqpURI := "amqp://user:bitnami@localhost:5672/"
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)
	return amqp.NewPublisher(amqpConfig, watermill.NewStdLogger(false, false))
}
