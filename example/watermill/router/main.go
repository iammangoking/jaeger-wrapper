package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/ThreeDotsLabs/watermill/message/router/plugin"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/watermilltracing"
)

// Original watermill example, check:
// https://github.com/ThreeDotsLabs/watermill/blob/master/_examples/basic/3-router/main.go

var (
	logger = watermill.NewStdLogger(false, false)
)

func main() {
	// Jaeger init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	router, err := message.NewRouter(message.RouterConfig{}, logger)
	if err != nil {
		panic(err)
	}

	router.AddPlugin(plugin.SignalsHandler)

	router.AddMiddleware(
		// wrapper for middlewares to collect error
		watermilltracing.WrapMiddlewares(

			middleware.CorrelationID,

			// StartSpan when doing retry
			watermilltracing.TraceWithSpan(
				middleware.Retry{
					MaxRetries:      3,
					InitialInterval: time.Millisecond * 100,
					Logger:          logger,
				}.Middleware,
			),

			middleware.Recoverer,

			// Test retry
			watermilltracing.TraceWithSpan(middleware.RandomFail(0.7)),
		)...,
	)

	// pubSub := gochannel.NewGoChannel(gochannel.Config{}, logger)

	// amqp pubsub
	// to deploy, check: https://hub.docker.com/r/bitnami/rabbitmq/
	amqpURI := "amqp://user:bitnami@0.0.0.0:5672/"
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)
	var pub message.Publisher
	var sub message.Subscriber
	pub, _ = amqp.NewPublisher(amqpConfig, logger)
	sub, _ = amqp.NewSubscriber(amqpConfig, logger)
	// tracing wrapper
	pub = watermilltracing.WrapPub(pub)
	sub = watermilltracing.WrapSub(sub)

	// go publishMessages(pub)

	// Http handler to producing messages
	go func(publisher message.Publisher) {
		http.HandleFunc("/ping", httptracing.MiddlewareFunc(
			func(w http.ResponseWriter, r *http.Request) {

				// use watermilltracing.NewMessage to inject jaeger context
				msg := watermilltracing.NewMessage(r.Context(), watermill.NewUUID(), []byte("Hello, world!"))
				middleware.SetCorrelationID(watermill.NewUUID(), msg)

				log.Printf("sending message %s, correlation id: %s\n", msg.UUID, middleware.MessageCorrelationID(msg))

				if err := publisher.Publish("incoming_messages_topic", msg); err != nil {
					panic(err)
				}

				fmt.Fprintf(w, "pong!")
			},
		))

		http.ListenAndServe(":8080", nil)
	}(pub)

	handler := router.AddHandler(
		"struct_handler",
		"incoming_messages_topic",
		sub,
		"outgoing_messages_topic",
		pub,
		structHandler{}.Handler,
	)

	handler.AddMiddleware(func(h message.HandlerFunc) message.HandlerFunc {
		return func(message *message.Message) ([]*message.Message, error) {
			fmt.Println("executing handler specific middleware for ", message.UUID)

			return h(message)
		}
	})

	router.AddNoPublisherHandler(
		"print_incoming_messages",
		"incoming_messages_topic",
		sub,
		printMessages(),
	)

	router.AddNoPublisherHandler(
		"print_outgoing_messages",
		"outgoing_messages_topic",
		sub,
		printMessages(),
	)

	ctx := context.Background()
	if err := router.Run(ctx); err != nil {
		panic(err)
	}
}

// func publishMessages(publisher message.Publisher) {
// 	for {
// 		msg := message.NewMessage(watermill.NewUUID(), []byte("Hello, world!"))
// 		middleware.SetCorrelationID(watermill.NewUUID(), msg)

// 		log.Printf("sending message %s, correlation id: %s\n", msg.UUID, middleware.MessageCorrelationID(msg))

// 		if err := publisher.Publish("incoming_messages_topic", msg); err != nil {
// 			panic(err)
// 		}

// 		time.Sleep(time.Second)
// 	}
// }

func printMessages() func(msg *message.Message) error {
	return func(msg *message.Message) error {
		fmt.Printf(
			"\n> Received message: %s\n> %s\n> metadata: %v\n\n",
			msg.UUID, string(msg.Payload), msg.Metadata,
		)

		return nil
	}
}

type structHandler struct{}

func (s structHandler) Handler(msg *message.Message) ([]*message.Message, error) {
	fmt.Println("structHandler received message", msg.UUID)

	// msg = message.NewMessage(watermill.NewUUID(), []byte("message produced by structHandler"))

	// use watermilltracing.NewMessage to inject jaeger context
	msg = watermilltracing.NewMessage(msg.Context(), watermill.NewUUID(), []byte("message produced by structHandler"))

	return message.Messages{msg}, nil
}
