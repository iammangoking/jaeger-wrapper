package watermillexample

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/watermilltracing"
)

const topic = "example.topic"

func Run(suber message.Subscriber, puber message.Publisher) {
	// sub worker
	go func() {
		// Jaeger init
		jaeger.InitClient(example.Config(), &log.Logger)
		defer jaeger.Close()

		// Sub
		subscriber := watermilltracing.WrapSub(suber)

		messages, err := subscriber.Subscribe(context.Background(), topic)
		if err != nil {
			panic(err)
		}

		isRetry := false

		handler := func(ctx context.Context, msg *message.Message) error {
			log.Printf("received message: %s, payload: %s", msg.UUID, string(msg.Payload))
			isRetry = !isRetry
			if isRetry {
				return errors.New("you have an error")
			}
			return nil
		}
		for msg := range messages {
			if handler(msg.Context(), msg) == nil {
				msg.Ack()
			} else {
				msg.Nack()
			}
		}
	}()

	// Pub
	publisher := watermilltracing.WrapPub(puber)

	// handler
	http.HandleFunc("/ping", httptracing.MiddlewareFunc(
		func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()

			key := xid.New().String()
			msg := watermilltracing.NewMessage(ctx, watermill.NewUUID(), []byte("Hello, world! "+key))

			publisher.Publish(topic, msg)

			fmt.Fprintf(w, "pong! "+key)
		},
	))

	http.ListenAndServe(":8080", nil)
}
