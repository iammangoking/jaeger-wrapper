package example

import (
	"github.com/rs/zerolog"

	"gitlab.com/iammangoking/jaeger-wrapper/config"
)

func Config() *config.Config {
	zerolog.SetGlobalLevel(zerolog.TraceLevel)

	// config
	cfg := &config.Config{
		ServiceName: "example",
		Reporter: &config.ReporterConfig{
			LogSpans: true,
			// LocalAgentHostPort: "localhost:6831",
			CollectorEndpoint: "http://localhost:14268/api/traces",
		},
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
	}

	// disable jaeger when cfg is nil
	// cfg = nil

	return cfg
}
