package main

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gormv2tracing"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func main() {
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	dsn := "root:123456@tcp(localhost:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.Use(gormv2tracing.Plugin())
	err = db.Debug().AutoMigrate(&Product{})
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	sp := opentracing.StartSpan("gormv2")
	defer sp.Finish()

	ctx = opentracing.ContextWithSpan(ctx, sp)
	session := db.WithContext(ctx)

	// Create
	session.Debug().Create(&Product{Code: "D42", Price: 100})
	// Read
	var product Product
	session.Debug().First(&product, 1)
	session.Debug().First(&product, "code = ?", "D42")
	// Update
	session.Debug().Model(&product).Update("Price", 200)
	// Update
	session.Debug().Model(&product).Updates(Product{Price: 200, Code: "F42"})
	session.Debug().Model(&product).Updates(map[string]interface{}{"Price": 200, "Code": "F42"})
	//
	// Delete
	session.Debug().Delete(&product, 1)
	session.Debug().Transaction(func(tx *gorm.DB) error {
		session.Debug().Clauses(
			clause.Locking{
				Strength: "UPDATE",
				Table:    clause.Table{Name: clause.CurrentTable},
			},
		).First(&product, 1)

		session.Debug().Clauses(
			clause.Locking{
				Strength: "SHARE",
				Table:    clause.Table{Name: clause.CurrentTable},
			},
		).First(&product, 1)
		return nil
	})
}
