package repository

import (
	"context"

	"github.com/jinzhu/gorm"

	"gitlab.com/iammangoking/jaeger-wrapper/example/gormv1withCA/model"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gormv1tracing"
)

func NewRepo(db gormv1tracing.DB) *repo {
	return &repo{db: db}
}

type repo struct {
	db gormv1tracing.DB
}

func (r *repo) Create(ctx context.Context) {
	r.db.Write(ctx).Debug().Create(&model.Product{Code: "D42", Price: 100})
}

func (r *repo) Update(ctx context.Context, p *model.Product) {
	r.db.Write(ctx).Debug().Model(&model.Product{}).Update("Price", 200)
	r.db.Write(ctx).Debug().Model(&model.Product{}).Updates(model.Product{Price: 200, Code: "F42"})
	r.db.Write(ctx).Debug().Model(&model.Product{}).Updates(map[string]interface{}{"Price": 200, "Code": "F42"})
}

func (r *repo) Read(ctx context.Context, p *model.Product) {
	r.db.Read(ctx).Debug().First(p, 1)
	r.db.Read(ctx).Debug().First(p, "code = ?", "D42")
}

func (r *repo) Delete(ctx context.Context, p *model.Product) {
	r.db.Write(ctx).Debug().Delete(p, 1)
}

func (r *repo) Transaction(ctx context.Context, f func(tx *gorm.DB) error) error {
	return r.db.Write(ctx).Debug().Transaction(f)
}
