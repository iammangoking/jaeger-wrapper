package main

import (
	"context"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gormv1withCA/model"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gormv1withCA/repository"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gormv1tracing"
)

func main() {
	// Init jaeger
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	// Init db
	db, err := gorm.Open("sqlite3", "example.db")
	if err != nil {
		panic(err)
	}
	db.Debug().AutoMigrate(&model.Product{})

	// repo
	repo := repository.NewRepo(gormv1tracing.Wrap(db, db))

	// Root span
	sp := opentracing.StartSpan("gormv1")
	defer sp.Finish()
	ctx := opentracing.ContextWithSpan(context.Background(), sp)

	// Create
	repo.Create(ctx)
	// Read
	var p model.Product
	repo.Read(ctx, &p)
	// Update
	repo.Update(ctx, &p)
	// Delete
	repo.Delete(ctx, &p)

	// Transaction
	repo.Transaction(ctx, func(tx *gorm.DB) error {
		tx.Debug().First(&p, 1)
		return nil
	})
}
