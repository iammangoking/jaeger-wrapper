package main

import (
	"context"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
)

func main() {
	// init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	for i := 0; i < 10; i++ {
		sp := opentracing.GlobalTracer().StartSpan("start example work")
		spCtx := opentracing.ContextWithSpan(context.Background(), sp)
		func(spCtx context.Context) {
			for i := 0; i < 6; i++ {
				span, _ := opentracing.StartSpanFromContext(spCtx, "work")
				time.Sleep(time.Millisecond)
				defer span.Finish()
				sp.SetTag("worker ", i)
				spCtx = opentracing.ContextWithSpan(spCtx, span)
			}
		}(spCtx)
		sp.Finish()
	}

}
