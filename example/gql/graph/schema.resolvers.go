package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"

	"github.com/rs/xid"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gql/graph/generated"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gql/graph/model"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gql/schema"
)

func (r *queryResolver) Ping(ctx context.Context) (*model.Pong, error) {
	return &model.Pong{
		ID: xid.New().String(),
	}, nil
}

func (r *queryResolver) User(ctx context.Context) (*schema.User, error) {
	// query from db with preload
	return &schema.User{
		ID:         "1",
		Name:       "JJ",
		BestFriend: "3",
		Friends: []schema.User{
			{
				ID:         "2",
				BestFriend: "1",
				Name:       "Alice",
				// preload if required
				Friends: []schema.User{},
			},
			{
				ID:         "3",
				BestFriend: "2",
				Name:       "Bob",
			},
			{
				ID:         "4",
				BestFriend: "3",
				Name:       "Charlie",
			},
		},
	}, nil
}

func (r *userResolver) Bestfriend(ctx context.Context, obj *schema.User) (*schema.User, error) {
	for _, v := range obj.Friends {
		if v.ID == obj.BestFriend {
			rv := v
			return &rv, nil
		}
	}
	return nil, errors.New("no friend")
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type queryResolver struct{ *Resolver }
type userResolver struct{ *Resolver }
