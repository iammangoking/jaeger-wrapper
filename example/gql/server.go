package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gql/graph"
	"gitlab.com/iammangoking/jaeger-wrapper/example/gql/graph/generated"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/gqltracing"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
)

const defaultPort = "8080"

func main() {
	// init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	srv.Use(gqltracing.Tracer{})

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", httptracing.Middleware(srv))

	fmt.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	http.ListenAndServe(":"+port, nil)
}
