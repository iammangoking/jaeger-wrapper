package schema

type User struct {
	ID         string
	Name       string
	BestFriend string
	Friends    []User
}
