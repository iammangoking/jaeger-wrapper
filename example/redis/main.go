package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog/log"

	"gitlab.com/iammangoking/jaeger-wrapper"
	"gitlab.com/iammangoking/jaeger-wrapper/example"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/httptracing"
	"gitlab.com/iammangoking/jaeger-wrapper/pkg/redistracing"
)

func main() {
	// init
	jaeger.InitClient(example.Config(), &log.Logger)
	defer jaeger.Close()

	// init redis
	client := redis.NewClient(&redis.Options{})

	// wrap redis
	redistracing.Wrap(client)

	// handler
	http.HandleFunc("/ping", httptracing.MiddlewareFunc(
		func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()

			client.Set(ctx, "pong", 1, time.Millisecond*10)
			client.Get(ctx, "pong")

			client.Pipelined(ctx, func(pipeliner redis.Pipeliner) error {
				pipeliner.Set(ctx, "pong2", 7, time.Second)
				pipeliner.Get(ctx, "pong2")
				return nil
			})

			fmt.Fprintf(w, "pong!")
		}))
	http.ListenAndServe(":8080", nil)
}
